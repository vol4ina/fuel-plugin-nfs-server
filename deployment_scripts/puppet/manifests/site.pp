notice('PLUGIN nfs-server: nfs-server.pp')

$nfs_storage_network_range  = hiera('storage_network_range',{})

class {'nfs_server':
  nfs_storage_network_range => $nfs_storage_network_range,
}