class nfs_server (
  $nfs_storage_network_range,
){
  include nfs_server::params

  # Install package and start services
  package { $nfs_server::params::package_name:
    ensure  => present,
  } ->
  file { $nfs_server::params::nfs_server_conf:
    ensure  => file,
    content => template('nfs_server/nfs_exports.erb'),
  } ~>
  service { $nfs_server::params::service_name:
    ensure  => running,
  }

  file{ '/var/nfs':
    ensure  => 'directory',
    owner   => 'nobody',
    group   => 'nogroup',
    mode    =>  '0777',
  }
}