class nfs_server::params {
  $nfs_server_conf = '/etc/exports'

  if $::osfamily == 'Debian' {

    $required_packages      = [ 'nfs-kernel-server' ]
    $package_name           = [ 'nfs-kernel-server' ]
    $service_name        = 'nfs-kernel-server'
    $nfs_service_name    = 'nfs-kernel-server'
    #
    # } elsif($::osfamily == 'RedHat') {

    #   $required_packages      = [ 'rpcbind', 'nfs-utils', 'nfs-utils-lib',
    #     'libevent', 'libtirpc', 'libgssglue' ]
    #   $package_name           = 'openstack-nova-compute'
    #   $service_name    		= 'openstack-nova-compute'
    #   $nfs_service_name 		= ['rpcbind']
    #
    # }
  }else {
    fail("unsuported osfamily ${::osfamily}, currently Debian and Redhat are the only supported platforms")
  }
}